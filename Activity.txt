Changing the selected region in the management console will not affect the listed instances in your account.
Answer: FALSE

What is the meaning of PEM in a PEM file?
Answer: Privacy Enhanced Mail

How many instance types does Amazon EC2 service have?
Answer: 606

AWS stands for?
Answer: Amazon Web Services

VM stands for?
Answer: Virtual Machine

What is the name of the instance type we used to create a new instance?
Answer: t2.micro

Refers to on-demand availability of computing-related resources such as servers, storage and databases without direct hardware management by the user.
Answer: Cloud computing

Contains a variety of services that can be used together in order to quickly build and deploy applications.
Answer: Cloud computing

Refers to a service (specifically, Platform as a Service or PaaS) offered to businesses both large and small.
Answer: Platform as a service (PaaS)

What is an alternative term for virtual machine within your own machine?
Answer: Guest Computer

It is a term used within AWS to refer to virtual machines.
Answer: Instances

How many total regions do AWS offer in its EC2 service?
Answer: 5


--USE AWS EC2 CALCULATOR FOR THIS PART--
LINK: calculator.aws/#/createCalculator/EC2
OBJECTIVE: Get the monthly pricing for the specified specifications.

Osaka region, Windows Server operating system, 2 vCPUs, 8 GiB memory
Instance type name: t3a.large
Monthly pricing: 65.26 USD

Ohio region, Linux operating system, 2 vCPUs, 8 GiB memory
Instance type name: t4g.large
Monthly pricing: 24.38 USD

Stockholm region, Linux with SQL Server Web operating system, 4 vCPUs, 16 GiB memory
Instance type name: t4g.xlarge
Monthly pricing: 176.08 USD

Singapore region, SUSE Linux Enterprise Server operating system, 8 vCPUs, 32 GiB memory
Instance type name: t4g.2xlarge
Monthly pricing: 160.02 USD





